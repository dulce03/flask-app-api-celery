from copy import deepcopy
import unittest
import json

import app

BASE_URL = 'http://127.0.0.1:5000/user'
BAD_ITEM_URL = '{}/90'.format(BASE_URL)
GOOD_ITEM_URL = '{}/4'.format(BASE_URL)
GOOD_ITEM_URL2 = '{}/5'.format(BASE_URL)


class TestFlaskApi(unittest.TestCase):
    def setUp(self):
        self.app = app.app.test_client()
        self.app.testing = True

    def test_get_all(self):
        response = self.app.get(BASE_URL)
        data = json.loads(response.get_data())
        self.assertEqual(response.status_code, 200)

    def test_get_one(self):
        response = self.app.get(BASE_URL)
        data = json.loads(response.get_data())
        self.assertEqual(response.status_code, 200)

    def test_item_not_exist(self):
        response = self.app.get(BAD_ITEM_URL)
        self.assertEqual(response.status_code, 200)

    def test_post(self):
        # wrong data fields = bad
        d = {"username": "some_item"}
        response = self.app.post(BASE_URL,
                                 data=json.dumps(d),
                                 content_type='application/json')
        self.assertEqual(response.status_code, 500)

        # missing value field email = bad
        d = {"username": "some_item"}
        response = self.app.post(BASE_URL,
                                 data=json.dumps(d),
                                 content_type='application/json')
        self.assertEqual(response.status_code, 500)

        # missing value field email2
        d = {"username": "screen", "email": 'string'}
        response = self.app.post(BASE_URL,
                                 data=json.dumps(d),
                                 content_type='application/json')
        self.assertEqual(response.status_code, 500)

        # valid: all required fields ok
        d = {"username": "screen", "email": "screen@testmail.com", "email2": "screen@mailtest.com"}
        response = self.app.post(BASE_URL,
                                 data=json.dumps(d),
                                 content_type='application/json')
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.get_data())

        # cannot add item with same data values again
        d = {"username": "screen", "email": "screen@testmail.com", "email2": "screen@mailtest.com"}
        response = self.app.post(BASE_URL,
                                 data=json.dumps(d),
                                 content_type='application/json')
        self.assertEqual(response.status_code, 500)

    def test_update(self):
        d = {"username": "test1", "email": "test1@test.com", "email2": "test1@mail.com"}
        response = self.app.put(GOOD_ITEM_URL,
                                data=json.dumps(d),
                                content_type='application/json')
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.get_data())

    def test_update_error(self):
        # cannot edit non-existing item
        d = {"username": "test2", "email": "test2@test.com", "email2": "test2@mail.com"}
        response = self.app.put(BAD_ITEM_URL,
                                data=json.dumps(d),
                                content_type='application/json')
        self.assertEqual(response.status_code, 500)

    def test_delete(self):
        response = self.app.delete(GOOD_ITEM_URL2)
        self.assertEqual(response.status_code, 200)
        response = self.app.delete(BAD_ITEM_URL)
        self.assertEqual(response.status_code, 500)

    def tearDown(self):
        # reset app, in this case nothing to do.
        print("Bye!!")

if __name__ == '__main__':
    unittest.main()
