from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from datetime import timedelta
from random import choice
from celery import Celery

import os


app = Flask(__name__)
basedir = os.path.abspath(os.path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'api_db.sqlite')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['CELERY_BROKER_URL'] = 'redis://localhost:6379'
app.config['CELERY_RESULT_BACKEND'] = 'redis://localhost:6379'
app.config['CELERYBEAT_SCHEDULE'] = {
        'run-every-15-second': {
            'task': 'app.add_user',
            'schedule': timedelta(seconds=15)
        },
        'run-every-1-minute': {
            'task': 'app.del_user',
            'schedule': timedelta(seconds=60)
        },
    }


# celery make_celery def
def make_celery(app):
    celery = Celery(
        app.import_name,
        backend=app.config['CELERY_RESULT_BACKEND'],
        broker=app.config['CELERY_BROKER_URL']
    )
    celery.conf.update(app.config)

    class ContextTask(celery.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    celery.Task = ContextTask
    return celery


celery = make_celery(app)
db = SQLAlchemy(app)
ma = Marshmallow(app)


# db table defination
class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(15), unique=True)
    email = db.Column(db.String(125), unique=True)
    email2 = db.Column(db.String(125))

    def __init__(self, username, email, email2):
        self.username = username
        self.email = email
        self.email2 = email2


class UserSchema(ma.Schema):
    class Meta:
        # Fields to expose
        fields = ('username', 'email', 'email2')


user_schema = UserSchema()
users_schema = UserSchema(many=True)


# endpoint to create new user
@app.route("/user", methods=["POST"])
def add_user():
    username = request.json['username']
    email = request.json['email']
    email2 = request.json['email2']

    new_user = User(username, email, email2)

    db.session.add(new_user)
    db.session.commit()
    return jsonify("Added a new user")


# endpoint to show all users
@app.route("/user", methods=["GET"])
def get_user():
    all_users = User.query.all()
    result = users_schema.dump(all_users)
    return jsonify(result.data)


# endpoint to get user detail by id
@app.route("/user/<id>", methods=["GET"])
def user_detail(id):
    user = User.query.get(id)
    return user_schema.jsonify(user)


# endpoint to update user
@app.route("/user/<id>", methods=["PUT"])
def user_update(id):
    user = User.query.get(id)
    username = request.json['username']
    email = request.json['email']
    email2 = request.json['email2']

    user.email = email
    user.email2 = email2
    user.username = username

    db.session.commit()
    return jsonify("Updated user info")


# endpoint to delete user
@app.route("/user/<id>", methods=["DELETE"])
def user_delete(id):
    user = User.query.get(id)
    db.session.delete(user)
    db.session.commit()
    return jsonify("Deleted user data")


# endpoint to create new user using random data
@app.route("/user/randomic", methods=["GET"])
def add_user_random():
    z = 5
    domains = [ "hotmail.com", "gmail.com", "aol.com", "protonmail.com" , "tutona.com", "yahoo.com"]
    domains2 = [ "rediffmail.com", "msn.com", "outlook.com", "amazon.com" , "rattler.com"]

    for i in range(z):
        username = ''.join(choice('aeiou') for i in range(5))
        email = username +'@'+choice(domains)
        email2 = username +'@'+choice(domains2)
        new_user = User(username, email, email2)
        db.session.add(new_user)

    db.session.commit()
    return jsonify("Added "+ str(z) +" user")


@celery.task()
def add_user():
    add_user_random()
    print('Addin new users')

@celery.task()
def del_user():
    db.session.execute('Delete from user where id IN(Select id from user order by id limit 15)')
    db.session.commit()
    print('Deleted users records which was added a minute ago')

if __name__ == '__main__':
    app.run(debug=True)
